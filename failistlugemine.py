
def get_line_amount():
    ridade_arv = 0
    with open("fail2.txt", encoding="UTF-8") as fail:
        for line in fail:
            ridade_arv += 1
    return ridade_arv

def get_word_amount():
    sonade_arv = 0
    with open("fail3.txt", encoding="UTF-8") as fail:
        for line in fail:
            line = line.split()
            sonade_arv += len(line)
    return sonade_arv


def get_tere_amount():
    sonade_arv = 0
    with open("fail4.txt", encoding="UTF-8") as fail:
        for line in fail:
            line = line.split()
            for sona in line:
                if sona.lower() == "tere":
                    print(sona)
                    sonade_arv += 1
    return sonade_arv


def print_words_with_less_than_4_letters():
    with open("fail5.txt", encoding="UTF-8") as fail:
        for line in fail:
            line = line.split()
            for sona in line:
                if len(sona) < 4:
                    print(sona)


def get_word_count_that_ends_with_e():
    amt_of_words = 0
    with open("fail6.txt", encoding="UTF-8") as file:
        for line in file:
            line = line.split()
            for sona in line:
                if sona[-1] == "e":
                    amt_of_words += 1
    return amt_of_words

def palju_kokku_kulutanud():
    """See funktsioon leiab kõik miinusmärgiga read."""

    kokku_kulutanud = 0
    with open("fail9.txt", encoding="UTF-8") as file:
        for line in file:
            line = line.split()
            # + 350 palk --> ["+", "350", "palk"]
            if line[0] == "-":
                kokku_kulutanud += int(line[1])
    return kokku_kulutanud


def palju_kokku_teeninud():
    """See funktsioon leiab kõik miinusmärgiga read."""

    kokku_teeninud = 0
    with open("fail9.txt", encoding="UTF-8") as file:
        for line in file:
            line = line.split()
            # + 350 palk --> ["+", "350", "palk"]
            if line[0] == "+":
                kokku_teeninud += int(line[1])
    return kokku_teeninud


def palju_plussi_voi_miinusesse():
    return palju_kokku_teeninud() - palju_kokku_kulutanud()


def viimane_kulutus():
    kulutused = []
    with open("fail9.txt", encoding="UTF-8") as file:
        for line in file:
            line = line.split()
            if line[0] == "-":
                # - 250 üür --> ["-", "250", "üür"]
                kulutused.append(line)
        return kulutused[-1]


def mitu_korda_saanud_palka():
    kokku_palka = 0
    with open("fail9.txt", encoding="UTF-8") as file:
        for line in file:
            line = line.split()
            # - 250 üür --> ["-", "250", "üür"]
            if line[2].lower() == "palk":
                kokku_palka += 1
    return kokku_palka

if __name__ == '__main__':
    print(palju_kokku_kulutanud())
    print(palju_kokku_teeninud())
    print(palju_plussi_voi_miinusesse())
    print(viimane_kulutus())
    print(mitu_korda_saanud_palka())