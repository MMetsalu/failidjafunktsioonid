Õhtul olin voodis juba,
nägin unes oma tuba.
Uksel seisis väike mees,
lumehelbed habemes,
sosistades mulle lausus:
"Vabandage, teie kõrgeausus."
Jõuluvana juba teel,
kingipakk on põdrareel.
Kingituse kätte saad,
siis kui salmid loetud saad.
Panin kähku selga kleidi,
et veel harjutada veidi.