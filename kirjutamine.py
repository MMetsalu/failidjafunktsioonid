import random

def genereeri_parool(parooli_pikkus: int, voimalik_symbolid: bool, voimalik_numbrid: bool):
    """Genereerib tugeva parooli."""
    parool = ""
    numbrid = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    symbolid = ["~", "!", "#", "$", "€", ";", ":", "/", "_", "?", "(", ")", "[", "]"]
    tahestik = "abcdefghijklmnopqrstuvwõäöüxy"
    for i in range(0, parooli_pikkus):
        if voimalik_numbrid and voimalik_symbolid:
            r = random.choice([0, 1, 2])
        elif voimalik_numbrid and not voimalik_symbolid:
            r = random.choice([0, 2])
        elif voimalik_symbolid and not voimalik_numbrid:
            r = random.choice([1, 2])
        else:
            r = 2
        if r == 0:
            r2 = random.choice(numbrid)
            parool += str(r2)
        elif r == 1:
            r2 = random.choice(symbolid)
            parool += r2
        elif r == 2:
            r2 = random.randint(0, len(tahestik) - 1)
            taht = tahestik[r2]
            r3 = random.randint(0,1) # teeb random arvu 0 või 1
            if r3 == 0:
                parool += taht
            else:
                parool += taht.upper()
    return parool

if __name__ == '__main__':
    print(genereeri_parool(20, True, True))
    print("a") # 65