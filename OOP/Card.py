class Card:
    """A class that represents a card."""
    suit = ""
    rank = ""

    def __init__(self, rank, suit):
        """Initialise the object"""
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return "Card: " + self.rank + " of " + self.suit
