class CardDeck:
    """A Card Deck"""

    cards = []

    def __init__(self):
        pass

    def add_card(self, card):
        """Adds a card to this Card Deck"""
        self.cards.append(card)

    def remove_card(self, card):
        """Removes a card from this Card Deck"""
        self.cards.remove(card)

    def shuffle_deck(self):
        pass

    def remove_card(self, position):
        pass

    def peek_top_card(self):
        pass

    def peek_card(self, position):
        pass
