class Point:
    """
    This class represents a Point.
    A point has too coordinates: x and y
    Both coordinates are integers
    Examples: (1, 3) or (-5, 2)
    """


class Point3D:
    """
    This class also represents a Point.
    A 3D point has 3 coordinates: x, y, z
    All coordinates are integers
    Examples: (1, 5, 3) and (-3, 2, 0)
    """
