class Bike:
    """
    This represents a simple bike.
    A bike has 2 wheels. It has a manufacturer.
    It has a manufacturing date.
    A bike can ride. It keeps track of how much it has ridden.
    A bike has a pin code.
    """