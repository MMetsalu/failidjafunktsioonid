class Dog:
    """
    This class is a representation of a Dog.
    A Dog is also an Animal.
    Dogs have a breed and an owner (who is a Person)
    Dogs can bark, instead of just "eating", they "chew bones"
    Dogs can do a trick.
    """