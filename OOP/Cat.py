class Cat:
    """
    This class represents a Cat.
    A Cat is also an Animal.
    Cats have a breed and an owner (who is a Person)
    Cats can meow, instead of just "eating", they "eat fish"
    Cats can play.
    """