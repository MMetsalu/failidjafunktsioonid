class Animal:
    """
    This class represents an animal.
    We need to keep the following information for animals:
    age, name, vaccines, height, weight, type(bird or mammal or such)
    All animals can eat and sleep
    """