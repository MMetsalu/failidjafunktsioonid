kaik = "X"
game_table = [
    "", "", "",
    "", "", "",
    "", "", ""
]


def prindi_mangulaud():
    """Prindib ilusa mängulaua."""
    #   |    |
    # ------------
    #   |    |
    # ------------
    #   |    |
    print(game_table[0] + " | " + game_table[1] + " | " + game_table[2])
    print("------")
    print(game_table[3] + " | " + game_table[4] + " | " + game_table[5])
    print("------")
    print(game_table[6] + " | " + game_table[7] + " | " + game_table[8])


def make_move(position: int, x_or_o: str) -> bool:
    """
    Make a move on the board.
    If the move was successful, return True
    Else return False
    """
    if game_table[position] == "":
        game_table[position] = x_or_o
        return True
    else:
        return False


def game_is_over():
    """Checks if the game is over."""
    pass


def get_winner():
    """Get the winner."""
    pass


if __name__ == '__main__':
    print("""Tere tulemast mängima trips traps trulli!
    Käike saad sisestada numbritega 1-9.
    """)
    prindi_mangulaud()
    while not game_is_over():
        input("Sisesta käik (" + kaik + "): ")
